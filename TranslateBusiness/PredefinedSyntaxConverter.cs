﻿using Enums;
using Roslyn.Compilers.CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranslateBusiness.Managers;

namespace TranslateBusiness
{
    class PredefinedSyntaxConverter
    {
        PreDefinedSyntaxConvertionManager convertionManager = new PreDefinedSyntaxConvertionManager();
        public void GenerateGivenCodeBlock(StatementSyntax syntax, NativePlatformEnum platformType)
        {
            var translatedCode = string.Empty;
            switch (syntax.Kind)
            {
                case SyntaxKind.ForStatement:
                    translatedCode = convertionManager.TranslateSyntaxCode((ForStatementSyntax)syntax, platformType);
                    break;
                case SyntaxKind.IfStatement:
                    translatedCode = convertionManager.TranslateSyntaxCode((IfStatementSyntax)syntax, platformType);
                    break;
                case SyntaxKind.LocalDeclarationStatement:
                    translatedCode = convertionManager.TranslateSyntaxCode((LocalDeclarationStatementSyntax)syntax, platformType);
                    break;
                case SyntaxKind.ExpressionStatement:
                    translatedCode = convertionManager.TranslateSyntaxCode((ExpressionStatementSyntax)syntax, platformType);
                    break;
                case SyntaxKind.SwitchStatement:
                    translatedCode = convertionManager.TranslateSyntaxCode((SwitchStatementSyntax)syntax, platformType);
                    break;
                case SyntaxKind.ReturnStatement:
                    translatedCode = convertionManager.TranslateSyntaxCode((ReturnStatementSyntax)syntax, platformType);
                    break;
                default:
                    break;
            }
        }
    }
}

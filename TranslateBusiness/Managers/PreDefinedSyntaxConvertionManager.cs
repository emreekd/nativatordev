﻿using Enums;
using Roslyn.Compilers.CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranslateBusiness.Managers
{
    class PreDefinedSyntaxConvertionManager
    {
        public string TranslateSyntaxCode(ForStatementSyntax syntax, NativePlatformEnum platformType)
        {
            return string.Empty;
        }
        public string TranslateSyntaxCode(IfStatementSyntax syntax, NativePlatformEnum platformType)
        {
            return string.Empty;
        }
        public string TranslateSyntaxCode(LocalDeclarationStatementSyntax syntax, NativePlatformEnum platformType)
        {
            return string.Empty;
        }
        public string TranslateSyntaxCode(ExpressionStatementSyntax syntax, NativePlatformEnum platformType)
        {
            return string.Empty;
        }
        public string TranslateSyntaxCode(SwitchStatementSyntax syntax, NativePlatformEnum platformType)
        {
            return string.Empty;
        }
        public string TranslateSyntaxCode(ReturnStatementSyntax syntax, NativePlatformEnum platformType)
        {
            return string.Empty;
        }
    }
}

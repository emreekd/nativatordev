﻿using Enums;
using Roslyn.Compilers.CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranslateBusiness.Managers;
using Nativator.CodeAnalysis;

namespace TranslateBusiness
{
    class MobileFrameworkSyntaxConverter
    {
        MobileFrameworkSyntaxConvertionManager convertionManager = new MobileFrameworkSyntaxConvertionManager();
        public void GenerateGivenCodeBlock(StatementSyntax syntax, NativePlatformEnum platformType, CustomSyntaxKind syntaxKind)
        {
            var translatedCode = string.Empty;
            switch (syntaxKind)
            {
                case CustomSyntaxKind.NButtonDeclarationSyntax:
                    translatedCode = convertionManager.TranslateSyntaxCode((NButtonDeclarationSyntax)syntax, platformType);
                    break;
                case CustomSyntaxKind.NLabelDeclarationSyntx:
                    translatedCode = convertionManager.TranslateSyntaxCode((NLabelDeclarationSyntax)syntax, platformType);
                    break;
                default:
                    break;
            }
        }
    }
}

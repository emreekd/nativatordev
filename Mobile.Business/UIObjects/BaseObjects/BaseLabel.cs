﻿using Mobile.Business.UIObjects.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobile.Business.UIObjects.BaseObjects
{
    public abstract class BaseLabel : IBaseLabel
    {
        public BaseLabel(string ID)
        {
            this.ID = ID;
        }
        public string ID;
        public string Text;
        public void SetText(string value)
        {
            this.Text = value;
        }

        public string GetText()
        {
            return this.Text;
        }


        public string GetID()
        {
            return this.ID;
        }
    }
}

﻿using Mobile.Business.UIObjects.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobile.Business.UIObjects.BaseObjects
{
    public abstract class BaseView : IBaseView
    {
        public BaseView(string ID)
        {
            this.ID = ID;
        }
        public string ID;
        public string GetID()
        {
            return this.ID;
        }
    }
}

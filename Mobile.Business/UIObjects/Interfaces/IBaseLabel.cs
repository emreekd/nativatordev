﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobile.Business.UIObjects.Interfaces
{
    public interface IBaseLabel
    {
        void SetText(string value);
        string GetText();
        string GetID();
    }
}

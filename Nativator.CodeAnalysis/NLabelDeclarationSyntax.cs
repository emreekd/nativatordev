﻿using Roslyn.Compilers.CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nativator.CodeAnalysis
{
    public class NLabelDeclarationSyntax 
    {
        public string TypeName { get { return "NLabel"; } }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationUtilities
{
    class ConfigReader
    {
        public static string GetSwiftBaseCodeFile()
        {
            return ConfigurationManager.AppSettings["SwiftBaseCode"].ToString();
        }
        public static string GetAndroidBaseCodeFile()
        {
            return ConfigurationManager.AppSettings["AndroidBaseCode"].ToString();
        }
        public static string GetProjectPathBinDebugAddition()
        {
            return ConfigurationManager.AppSettings["ProjectDirectoryBinDebugAddition"].ToString();
        }
        public static string GetGeneratedSwiftCodePathAddition()
        {
            return ConfigurationManager.AppSettings["GeneratedSwiftCodePathAddition"].ToString();
        }
        public static string GetGeneratedSwiftCodePathAdditionWithBinDebug()
        {
            return ConfigurationManager.AppSettings["GeneratedSwiftCodePathAdditionWithBinDebug"].ToString();
        }
        public static string GetGeneratedAndoridCodePathAddition()
        {
            return ConfigurationManager.AppSettings["GeneratedAndroidCodePathAddition"].ToString();
        }
        public static string GetGeneratedAndroidCodePathAdditionWithBinDebug()
        {
            return ConfigurationManager.AppSettings["GeneratedAndroidCodePathAdditionWithBinDebug"].ToString();
        }
        public static string GetNativatorBinDebug { get { return ConfigurationManager.AppSettings["NativatorBinDebug"].ToString(); } }
        public static string GetNativatorCommonUtilitiesAndroid { get { return ConfigurationManager.AppSettings["NativatorCommonUtilitiesAndroid"].ToString(); } }
        public static string GetNativatorDevAndroidCodes { get { return ConfigurationManager.AppSettings["NativatorDevAndroidCodes"].ToString(); } }
        public static string GetNativatorDevAndroidCodesWithBinDebug { get { return ConfigurationManager.AppSettings["NativatorDevAndroidCodesWithBinDebug"].ToString(); } }
        public static string GetNativatorCommonUtilitiesSwift { get { return ConfigurationManager.AppSettings["NativatorCommonUtilitiesSwift"].ToString(); } }
        public static string GetNativatorDevSwiftCodes { get { return ConfigurationManager.AppSettings["NativatorDevSwiftCodes"].ToString(); } }
        public static string GetNativatorDevSwiftCodesWithBinDebug { get { return ConfigurationManager.AppSettings["NativatorDevSwiftCodesWithBinDebug"].ToString(); } }
        public static string GetViewsFolderPathAddition { get { return ConfigurationManager.AppSettings["ViewsFolderPathAddition"].ToString(); } }
        public static string CSharpFileExtension { get { return ConfigurationManager.AppSettings["CSharpFileExtension"].ToString(); } }
    }
}
